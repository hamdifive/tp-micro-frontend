import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderUpdateProductComponent } from './sender-update-product.component';

describe('SenderUpdateProductComponent', () => {
  let component: SenderUpdateProductComponent;
  let fixture: ComponentFixture<SenderUpdateProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderUpdateProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderUpdateProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
