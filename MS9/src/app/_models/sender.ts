import { Product } from './product';

export class Sender {
    _id:string;
    name:string;
    lastName:string;
    phoneNumber:String;
    cin:string;
    address:string;
    rib:string;
    email:string;
    password:string;
    banner:any;
    type:string;
    menu:Product[];
    times:Times[];
    minOrder: number;
    verified:boolean;
    closed:boolean;
    active:boolean;
    coordinates:{
		latitude:number,
		longitude:number
	}
    public constructor(init?: Partial<Sender >) {
        Object.assign(this, init);
    }



}
export class Times {
    _id:string;
    Day:number;
	DayofTheWeek:string;
	openningTime:string;
	closingTime:string;
	closed:boolean;
    public constructor(DayofTheWeek,day) {
        this.DayofTheWeek=DayofTheWeek;
        this.Day=day;
        this.closed=false;
    }


}
