import { Component ,OnInit} from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';



@Component({
  selector: 'delivery-man-footer-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'delivery-man-footer';
  yoshiUrl = assetUrl("yoshi.png");
  
  ngOnInit() {
  
  }
}