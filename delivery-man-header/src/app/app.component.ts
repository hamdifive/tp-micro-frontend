import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeliveryManService } from 'src/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/_models/deliveryMan';

@Component({
  selector: 'delivery-man-header-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit { 

  active:Boolean;
  delievryman:DeliveryMan= new DeliveryMan();
  constructor(public router: Router,private deliverymanService:DeliveryManService) { }

  ngOnInit() {
    
  }
  
}
